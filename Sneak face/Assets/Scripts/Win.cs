﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other) // checks if something collides with a meteor 
    {
        if (other.gameObject.tag == "Player") // checks for the certain tags
        {
            GameManager.instance.Win = !GameManager.instance.Win;
        }

    }
}
