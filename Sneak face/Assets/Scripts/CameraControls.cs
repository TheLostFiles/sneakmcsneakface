﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour
{
    public GameObject followPlayer;

    private Vector3 velocity = Vector3.zero;

    private float camZ;

    private new Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        camZ = transform.position.z;
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 delta = followPlayer.transform.position - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, camZ));
        Vector3 destination = transform.position + delta;
        destination.z = camZ;
        transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, 0);
    }
}
