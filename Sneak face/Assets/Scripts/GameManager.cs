﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Transform PTransform;
    public bool Win;
    public Text EndText;

    void Awake()
    {
        if (instance == null) // checks if the instance is null
        {
            instance = this; // Store THIS instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject); // Don't delete this object if we load a new scene
        }
        else
        {
            Destroy(this.gameObject); // There can only be one - this new object must die
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }

    }


    // Update is called once per frame
    void Update()
    {
        if(Win) // checks if the game over condition 
        {
            EndText.text = "You Win!!!"; // You Win!!!f
        }

    }
}
