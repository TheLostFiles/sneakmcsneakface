﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class Player : Controller
{
    private Rigidbody2D rb2d; //sets the RigidBody as rb2d
    private Transform tf;
    public Vector3 Growl;
    public bool Grr;
    public float Speed; // makes the Speed public
    public float RotationSpeed; // makes the Speed of the rotation public
    public float Sprint;
    public float Crouch;
    public int Volume;

    

    void Start()
    {
        GameManager.instance.PTransform.position = transform.position;
        Grr = false;
        tf = GetComponent<Transform>();
        rb2d = GetComponent<Rigidbody2D>(); // sets the name of the component so that I can use it easily 
    }

    void Update()
    {

        if (Input.GetKey("w")) //grabs the w key
        {
            MoveForward(Speed, rb2d, tf); // calls Move forward
            Volume = 2; // sets volume
        }

        if (Input.GetKey("s")) //grabs the s key
        {
            MoveBackwards(Speed, rb2d, tf); // calls Move Backwards
            Volume = 2;
        }

        if (Input.GetKey("d")) //grabs the d key
        {
            TurnRight(RotationSpeed, rb2d, tf); // calls turn right 
        }

        if (Input.GetKey("a")) //grabs the a key
        {
            TurnLeft(RotationSpeed, rb2d, tf); // calls turn Left
        }


        if (Input.GetKey("left shift")) //grabs the w key
        {
            if (Input.GetKey("w")) //grabs the w key
            {
                Sprinting(Sprint, Speed, rb2d, tf); // calls sprinting
                Volume = 5;
            }
        }

        if (Input.GetKey("left ctrl")) //grabs the w key
        {
            if (Input.GetKey("w")) //grabs the w key
            {
                Crouching(Crouch, Speed, rb2d, tf); // calls crouching
                Volume = 0;
            }

            if (Input.GetKey("s")) //grabs the w key
            {
                CrouchingBack(Crouch, Speed, rb2d, tf); // calls crouching but backwards

                Volume = 0;
            }
        }

        if (Input.GetKey("g")) // grabs the g key 
        {
            Volume = 10; // sets volume
            Growl = tf.position; // sets growl to a location
            Grr = true; // sets grr to true
        }

        if (!Input.anyKey) // checks if you are pressing any key
        {
            Volume = 0; // sets volume
        }

        
    }
}