﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    //--------//
    //Movement//
    //--------//
    public void MoveForward(float Speed, Rigidbody2D rb2d, Transform tf) 
    {
        rb2d.velocity = tf.up * Speed; // Moves Forwards
    }
    public void MoveBackwards(float Speed, Rigidbody2D rb2d, Transform tf) 
    {
        rb2d.velocity = -tf.up * Speed; // moves backwards
    }
    public void TurnRight(float RotationSpeed, Rigidbody2D rb2d, Transform tf) 
    { 
        transform.Rotate(new Vector3(0, 0, -2) * Time.deltaTime * RotationSpeed, Space.World); //spins your character
    }
    public void TurnLeft(float RotationSpeed, Rigidbody2D rb2d, Transform tf) 
    {
        transform.Rotate(new Vector3(0, 0, 2) * Time.deltaTime * RotationSpeed, Space.World);//spins your character
    }
    public void Sprinting(float Sprint, float Speed, Rigidbody2D rb2d, Transform tf) 
    {
        rb2d.velocity = transform.up * Speed * Sprint; // Sprint mechanic
    }
    public void Crouching(float Crouch, float Speed, Rigidbody2D rb2d, Transform tf) 
    {
        rb2d.velocity = transform.up * Speed * Crouch; // Crouching mechanic
    }
    public void CrouchingBack(float Crouch, float Speed, Rigidbody2D rb2d, Transform tf) 
    {
        rb2d.velocity = -transform.up * Speed * Crouch; // crouch mechanic

    }
}
