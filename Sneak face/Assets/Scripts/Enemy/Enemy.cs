﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Enemy : Controller
{
    [SerializeField] public LayerMask layermask; // makes a layermask

    private Rigidbody2D rb2d;
    public float Speed;
    public float SightDistance;
    private float PlayerDistance;

    public float TimeLeft;
    private float TimeLeft2;
    

    private PatrolPoints Ppoints;
    private int patrolpointIndex;
    public bool CanSee;

    private Vector3 V3;
    private Transform tf;
    public float fieldOfView = 45.0f;

    public Image Warning;

    void Start()
    {
        tf = GetComponent<Transform>(); // 
        rb2d = GetComponent<Rigidbody2D>();
        Ppoints = GameObject.FindGameObjectWithTag("PatrolPoints").GetComponent<PatrolPoints>();
        StartCoroutine(StateChange()); // starts the FSM for AI states
        Physics2D.queriesStartInColliders = false; 
        Physics2D.queriesHitTriggers = false;

    }

    public enum AiState // sets the states for the State machine
    {
        Patrol, // patrol
        Seek, // seek
        Attack, // attack

    }

    public AiState _state; // makes a state

    IEnumerator StateChange() // the state changing IEnumerator
    {
        while (true) // while
        {
            switch (_state) // the switch that controls the states
            {
                case AiState.Patrol: // patrol
                    DoPatrol();
                    break; // break

                case AiState.Seek: // seek
                    DoSeek();
                    break; // break

                case AiState.Attack: // attack 
                    DoAttack();
                    break; // break

            }

            yield return new WaitForSeconds(0); // waits for nothing
        }
    }

    public void DoPatrol() // the patrolling state
    {

        Warning.color = Color.green; // changes the color of the ring on the enemies

        Speed = 10f; // sets speed
        TimeLeft2 = TimeLeft; // sets time things

        V3 = (transform.position - Ppoints._PatrolPoints[patrolpointIndex].position).normalized; // finds the distance for the new patrol points
        transform.up = V3; // sets the up transform
        MoveBackwards(Speed, rb2d, tf); // moves enemies

        if (Vector2.Distance(transform.position, Ppoints._PatrolPoints[patrolpointIndex].position) < 0.5f) // checks if they are far enough apart
        {
            if (patrolpointIndex < Ppoints._PatrolPoints.Length - 1) // makes the number go up 
            {
                patrolpointIndex++; // adds to the index
            }
            else
            {
                patrolpointIndex = 0; // set it to 0 so they starts over
            }
        }

        if (GameObject.Find("Player").GetComponent<Player>().Volume == 10 && PlayerDistance <= 10) // checks for sound and distance
        {
            _state = AiState.Seek; // sets it to seek
        }

        if (GameObject.Find("Player").GetComponent<Player>().Volume == 5 && PlayerDistance <= 8)// checks for sound and distance
        {
            _state = AiState.Seek;// sets it to seek
        }

        if (GameObject.Find("Player").GetComponent<Player>().Volume == 2 && PlayerDistance <= 5)// checks for sound and distance
        {
            _state = AiState.Seek;// sets it to seek
        }

        if (CanSee) // checks if the bool for seeing is true
        {
            _state = AiState.Attack;// sets it to Attack
        }
    }




    public void DoSeek() // seeking State
    {
        Warning.color = Color.yellow; // changes the color of the warning to yellow

        Speed = 5f; // sets speed

        V3 = (transform.position - GameManager.instance.PTransform.position).normalized; // sets up hs as the position if the player
        transform.up = V3; // sets the transform up to hs

        if (GameObject.Find("Player").GetComponent<Player>().Grr) // checks for grr being used
        {
            V3 = (transform.position - GameObject.Find("Player").GetComponent<Player>().Growl).normalized; // sets new point of interest
            transform.up = V3; // sets transform up
            MoveBackwards(Speed, rb2d, tf); // moves

            if (Vector3.Distance(tf.position, GameObject.Find("Player").GetComponent<Player>().Growl) < 0.5f) // makes sure they don't get too close
            {
                GameObject.Find("Player").GetComponent<Player>().Grr = false; // sets it to false
            }

            if (GameObject.Find("Player").GetComponent<Player>().Grr == false) // checks of its false
            {
                _state = AiState.Patrol; // goes back to patrolling
            }
        }

        if (CanSee) // checks if it can see
        {
            _state = AiState.Attack; // attacks
        }

        if (!CanSee && GameObject.Find("Player").GetComponent<Player>().Volume == 0) // if it can't see and the volume is zero
        {
            _state = AiState.Patrol; // goes back to patrolling
        }



    }

    public void DoAttack() // Attacking State
    {
        Warning.color = Color.red; // sets warning color to red

        Speed = 8f; //sets speed

        V3 = (transform.position - GameManager.instance.PTransform.position) // changes V3
            .normalized; // sets up hs as the position if the player
        transform.up = V3; // sets the transform up to hs
        MoveBackwards(Speed, rb2d, tf); // moves

        if (!CanSee) // checks if he can't see
        {
            MoveBackwards(Speed, rb2d, tf); // moves

            TimeLeft2 -= Time.deltaTime; // makes a timer go off
            if (TimeLeft2 < 0) // checks that timer
            {
                _state = AiState.Patrol; // goes back to patrolling
            }

        }
    }




    // Update is called once per frame
    void Update()
    {
        PlayerDistance = Vector3.Distance(GameManager.instance.PTransform.position, tf.position); // sets the players distance

        Sight(); // calls sight
    }

    void Sight()
    {
        Vector3 Enemy2Player = GameManager.instance.PTransform.position - tf.position; 

        float angleToTarget = Vector3.Angle(Enemy2Player, -tf.up); 


        if (angleToTarget < fieldOfView) // checks if he is in that range
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, Enemy2Player, SightDistance, layermask); // sets out the raycast

            if (hitInfo.collider != null) // checks if it is hitting something
            {
                Debug.DrawLine(tf.position, hitInfo.point, Color.red); // sets red line
                CanSee = true; // flips bool
            }
            else
            {
                Debug.DrawLine(tf.position, tf.position + -tf.up * SightDistance, Color.black); // sets a black line
                CanSee = false; // flips bool
            }
        }
    }

    void OnCollisionEnter2D(Collision2D other) // checks if something collides with a Player
    {
        if (other.gameObject.tag == "Player") // checks for the certain tags
        {
            SceneManager.LoadScene("Menu"); // sends you back to the menu1
        }

    }
}
       
